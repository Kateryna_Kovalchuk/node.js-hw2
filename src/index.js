const express = require('express');
const app = express();
const morgan = require('morgan');
require('dotenv').config();
const mongoose = require('mongoose');

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(authMiddleware);

app.use('/api/users', authRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
    try{
        await mongoose.connect('mongodb+srv://root:root@cluster0.vxkm7.mongodb.net/test?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const port = process.env.PORT;

        app.listen(port);
    }catch (err){
        console.error(`Error on server startup: ${err.message}`);
    }
}
// await mongoose.connection.close();

start();
