const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId) => {
    const notes = await Note.find({userId});
    return notes;
}

const addNoteToUser = async (userId, notePayLoad) => {
    const note = new Note({...notePayLoad, userId});
    await note.save();
}

module.exports = {
    getNotesByUserId,
    addNoteToUser
}
