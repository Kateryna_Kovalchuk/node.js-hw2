const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');

const {
    signIn,
    registration
} = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        await registration({username, password});

        res.json({message: 'Account created successfully!'})
    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.post('/login',  async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        const token = await signIn({username, password});

        res.json({"jwt_token": token, message: 'Logged in successfully!'})
    } catch (err) {
        if (err) {
            res.status(400).json({message: err.message});
        }
    }
})

router.get('/me', async (req, res) => {
    try {
        const {username} = req.user;

        const {_id, username: name, createdDate} = await User.findOne({username});

        res.json({user: {_id, name, createdDate}})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.delete('/me', async (req, res) => {
    try {
        const {username} = req.user;
        await User.deleteOne({username});

        res.json({"message": "Success"});

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.patch('/me', async (req, res) => {
    try {
        const {username} = req.user;
        const user = await User.findOne({username});

        if (!(await bcrypt.compare(req.body.oldPassword, user.password))){

            throw new Error('Invalid password');
        }

        await User.updateOne({username}, {$set: {password: await bcrypt.hash(req.body.newPassword, 10)}})

        res.json({"message": "Success"});

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

module.exports = {
    authRouter: router
}
