const express = require('express');
const router = express.Router();
const {Note} = require('../models/noteModel');

const {
    getNotesByUserId,
    addNoteToUser
} = require('../services/notesService');

router.get('/',  async (req, res) => {
    try {
        const {userId} = req.user;

        const notes = await getNotesByUserId(userId);
        res.json({notes})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.get('/:id',  async (req, res) => {
    try {
        const {userId} = req.user;
        const notes = await getNotesByUserId(userId);
        const {id} = req.params;
        let note;
        for (const item of notes){
            if (item._id == id){

                note = item;
            }
        }
        res.json({note})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.post('/',  async (req, res) => {
    try {
        const {userId} = req.user;

        await addNoteToUser(userId, req.body);
        res.json({message: 'Note created successfully'});

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.put('/:id',  async (req, res) => {
    try {
        const {userId} = req.user;
        const {noteId} = req.params;
        await Note.updateOne({$and: [{userId}, {noteId}]}, req.body);
        res.json({"message": "Success"})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.patch('/:id',  async (req, res) => {
    try {
        const {userId} = req.user;
        const {noteId} = req.params;
        const note = await Note.find({$and: [{userId}, {noteId}]});
        await Note.updateOne({$and: [{userId}, {noteId}]}, {$set: {completed: !note.completed}});
        res.json({"message": "Success"})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

router.delete('/:id',  async (req, res) => {
    try {
        const {userId} = req.user;
        const {noteId} = req.params;
        // const note = await Note.find({$and: [{userId}, {noteId}]});
        await Note.deleteOne({$and: [{userId}, {noteId}]});
        res.json({"message": "Success"})

    } catch (err) {
        if (err) {
            res.status(500).json({message: err.message});
        }
    }
})

module.exports = {
    notesRouter: router
}
