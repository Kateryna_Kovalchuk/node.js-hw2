const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    completed: {
        type: Boolean,
        required: true,
        default: false
    },
    text: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        required: true,
        default: new Date().toISOString()
    }
});

module.exports = {
    Note
}
