const mongoose = require('mongoose');

const User = mongoose.model('User', {
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        unique: true
    },
    createdDate: {
        type: String,
        required: true,
        default: new Date().toISOString()
    }
});

module.exports = {
    User
}
